export interface TaskInterface {
  id?: string;
  title: string;
  date: string;
}
