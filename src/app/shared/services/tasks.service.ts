import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TaskInterface } from '../task-interface';
import { map } from 'rxjs/operators';
import { Observable, ObservableInput } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TasksService {
  private readonly url = 'http://localhost:3000/organizer';

  constructor(private http: HttpClient) {}

  create(task: TaskInterface): Observable<TaskInterface> {
    return this.http.post(`${this.url}`, task).pipe(
      map((res: TaskInterface) => {
        return res;
      })
    );
  }

  load(date: moment.Moment): Observable<TaskInterface[]> {
    return this.http.get<TaskInterface[]>(
      `${this.url}?date_like=${date.format('DD-MM-YYYY')}`
    );
  }

  remove(task) {
    return this.http.delete<void>(`${this.url}/${task.id}`);
  }

  loadMonthTasks(date: moment.Moment): Observable<TaskInterface[]> {
    return this.http.get<TaskInterface[]>(
      `${this.url}?date_like=${date.format('MM-YYYY')}`
    );
  }
}
