import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { switchMap } from 'rxjs/operators';
import { DateService } from '../shared/services/date.service';
import { TasksService } from '../shared/services/tasks.service';
import { TaskInterface } from '../shared/task-interface';
import { DayInterface } from './day-interface';
import { WeekInterface } from './week-interface';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
})
export class CalendarComponent implements OnInit {
  calendar: WeekInterface[];
  mTasks: TaskInterface[];
  startDay: moment.Moment;
  endDay: moment.Moment;
  now: moment.Moment;
  constructor(
    private dateService: DateService,
    private tasksService: TasksService
  ) {}

  ngOnInit(): void {
    this.dateService.date
      .pipe(
        switchMap((now) => {
          this.now = now;
          this.startDay = now.clone().startOf('month').startOf('week');
          this.endDay = now.clone().endOf('month').endOf('week');
          return this.tasksService.loadMonthTasks(now);
        })
      )
      .subscribe(
        (tasks: TaskInterface[]) => (this.calendar = this.generate(tasks))
      );
  }

  generate(tasks: TaskInterface[]): WeekInterface[] {
    this.mTasks = tasks;
    const date = this.startDay.clone().subtract(1, 'day');
    const calendar = [];
    while (date.isBefore(this.endDay, 'day')) {
      calendar.push({
        days: Array(7)
          .fill(0)
          .map(() => {
            const value = date.add(1, 'day').clone();
            const active = moment().isSame(value, 'date');
            const disabled = !this.now.isSame(value, 'month');
            const selected = this.now.isSame(value, 'date');
            const taskCount = this.getTaskCount(value);
            return {
              value,
              active,
              disabled,
              selected,
              taskCount,
            };
          }),
      });
    }
    return calendar;
  }

  select(day: DayInterface): void {
    this.dateService.changeDate(day.value);
  }

  getTaskCount(value: moment.Moment): number {
    let count = [];
    const cDay = value.format('DD-MM-YYYY');
    count = this.mTasks.filter((day) => cDay === day.date);
    return count.length > 0 ? count.length : null;
  }
}
