export interface DayInterface {
  value: moment.Moment;
  active: boolean;
  disabled: boolean;
  selected: boolean;
  taskCount: number;
}
