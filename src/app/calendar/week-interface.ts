import { DayInterface } from './day-interface';

export interface WeekInterface {
  days: DayInterface[];
}
