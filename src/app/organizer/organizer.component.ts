import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { switchMap } from 'rxjs/operators';
import { DateService } from '../shared/services/date.service';
import { TasksService } from '../shared/services/tasks.service';
import { TaskInterface } from '../shared/task-interface';

@Component({
  selector: 'app-organizer',
  templateUrl: './organizer.component.html',
  styleUrls: ['./organizer.component.scss'],
})
export class OrganizerComponent implements OnInit {
  tasks: TaskInterface[] = [];
  constructor(
    public dateService: DateService,
    private tasksService: TasksService
  ) {}
  form: FormGroup;
  ngOnInit(): void {
    this.initForm();
    this.dateService.date
      .pipe(switchMap((date) => this.tasksService.load(date)))
      .subscribe((tasks) => {
        this.tasks = tasks;
      });
  }

  initForm(): void {
    this.form = new FormGroup({
      title: new FormControl('', Validators.required),
    });
  }

  submit(): void {
    const { title } = this.form.value;
    const task: TaskInterface = {
      title,
      date: this.dateService.date.value.format('DD-MM-YYYY'),
    };
    this.tasksService.create(task).subscribe(
      (task) => {
        this.form.reset();
        this.tasks.push(task);
        this.dateService.refreshCalendarHandler();
      },
      (error) => {
        console.error(error);
      }
    );
  }

  removeTask(task: TaskInterface): void {
    this.tasksService.remove(task).subscribe(
      (data) => {
        this.tasks = this.tasks.filter((item) => task.id !== item.id);
        this.dateService.refreshCalendarHandler();
      },
      (error) => {
        console.error(error);
      }
    );
  }
}
